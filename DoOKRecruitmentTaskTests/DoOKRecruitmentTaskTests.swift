//
//  DoOKRecruitmentTaskTests.swift
//  DoOKRecruitmentTaskTests
//
//  Created by Tomasz Paluch on 16/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import XCTest
@testable import DoOKRecruitmentTask

class DoOKRecruitmentTaskTests: XCTestCase {
    var catsDataSource: FakedCatsDataSource!
    
    override func setUp() {
        catsDataSource = FakedCatsDataSource()
    }

    override func tearDown() {}
    
    func testGetCategoriesCount_Standard_Returns5() {
        catsDataSource.fetchCats { _ in }
        let cellCount = catsDataSource.catCount
        XCTAssertEqual(cellCount, 5)
    }
}
