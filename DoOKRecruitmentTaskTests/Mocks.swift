//
//  Mocks.swift
//  DoOKRecruitmentTaskTests
//
//  Created by Tomasz Paluch on 21/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import Foundation
@testable import DoOKRecruitmentTask

class FakedCatsDataSource: CatsDataSource {
    override init() {
        super.init(broker: FakedCatsBroker())
    }
}

class FakedCatsBroker: CatsBroker {
    override init() {
        super.init(provider: FakedCatsProvider())
    }
}

class FakedCatsProvider: CatsProvider {
    private let url: URL
    
    override init() {
        url = Bundle.main.url(forResource: "input", withExtension: "json")!
    }
    
    override func fetchCats(completion: @escaping (Result<Data, Error>) -> Void) {
        do {
            let data = try Data(contentsOf: url)
            completion(.success(data))
        } catch {
            print(error)
        }
    }
}
