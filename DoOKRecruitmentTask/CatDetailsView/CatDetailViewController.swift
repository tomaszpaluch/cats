//
//  CatDetailViewController.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 16/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import UIKit

class CatDetailViewController: UIViewController {
    var whenViewDisappers: (() -> Void)?
    var openWikipedia: (() -> Void)?
    
    private let scrollView: UIScrollView
    private let stackView: UIStackView

    private let imageView: UIImageView
    private let nameLabel: UILabel
    private let descriptionLabel: UILabel
    private let wikipediaButton: UIButton
    
    init(data: CatData, with image: UIImage?) {
        scrollView = UIScrollView(frame: .zero)
        stackView = UIStackView(frame: .zero)

        imageView = UIImageView(frame: .zero)
        nameLabel = UILabel(frame: .zero)
        descriptionLabel = UILabel(frame: .zero)
        wikipediaButton = UIButton(frame: .zero)
        
        stackView.axis = .vertical
        stackView.spacing = 10
            
        imageView.backgroundColor = UIColor.cyan.withAlphaComponent(0.4)
        imageView.image = image
        
        nameLabel.font = .systemFont(ofSize: 18, weight: .semibold)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .justified
        
        wikipediaButton.setTitle("Go to Wikipedia", for: .normal)
        wikipediaButton.setTitleColor(.white, for: .normal)
        wikipediaButton.backgroundColor = .systemBlue

        super.init(nibName: nil, bundle: nil)
        
        view.accessibilityIdentifier = "catDetailsView"
        view.backgroundColor = .systemBackground
        view.addSubview(scrollView)
        scrollView.addSubview(imageView)
        scrollView.addSubview(stackView)
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(wikipediaButton)
        
        wikipediaButton.addTarget(self, action: #selector(wikipediaButtonAction), for: .touchUpInside)
        
        setupScrollViewConstraints()
        setupImageViewConstraints(heightToWidthRatio: data.imageRatio)
        setupStackViewConstraints()
        
        if let names = data.names {
            nameLabel.text = names
            
            nameLabel.isHidden = false
        } else {
            nameLabel.isHidden = true
        }
        
        if let descriptions = data.descriptions {
            descriptionLabel.text = descriptions
            
            descriptionLabel.isHidden = false
        } else {
            descriptionLabel.isHidden = true
        }
        
        if let _ = data.wikiURL {            
            wikipediaButton.isHidden = false
        } else {
            wikipediaButton.isHidden = true
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func wikipediaButtonAction() {
        openWikipedia?()
    }
    
    private func setupScrollViewConstraints() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
    
    private func setupImageViewConstraints(heightToWidthRatio ratio: CGFloat) {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: ratio).isActive = true
        imageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: scrollView.rightAnchor).isActive = true
        
        scrollView.widthAnchor.constraint(equalTo: imageView.widthAnchor).isActive = true
    }
    
    private func setupStackViewConstraints() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 18).isActive = true
        stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -18).isActive = true
        stackView.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 18).isActive = true
        stackView.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -18).isActive = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        wikipediaButton.layer.masksToBounds = true
        wikipediaButton.layer.cornerRadius = wikipediaButton.frame.height / 2
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        whenViewDisappers?()
    }
    
    func reloadImage(with image: UIImage?) {
        imageView.image = image
    }
}
