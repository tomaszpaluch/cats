//
//  AppDelegate.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 16/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let rootViewController = CatsViewController()
//        let navigationController = UINavigationController(rootViewController: rootViewController)
        
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
        
        return true
    }
}

