//
//  CatCell.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 21/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import UIKit

class CatCell: UITableViewCell {
    private let mainImageView: UIImageView
    private let blurredEffectView: UIView
    private let nameLabel: UILabel
    
    private var mainImageViewHeightConstraint: NSLayoutConstraint?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        mainImageView = UIImageView(frame: .zero)
        let blurEffect = UIBlurEffect(style: .systemThinMaterialLight)
        blurredEffectView = UIVisualEffectView(effect: blurEffect)
        nameLabel = UILabel(frame: .zero)
        
        mainImageView.backgroundColor = UIColor.systemPink.withAlphaComponent(0.5)
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(mainImageView)
        addSubview(blurredEffectView)
        addSubview(nameLabel)
        
        setupImageViewConstraints()
        setupBlurredEffectViewConstraints()
        setupNameLabelConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupImageViewConstraints() {
        mainImageView.translatesAutoresizingMaskIntoConstraints = false
        mainImageView.topAnchor.constraint(equalTo: topAnchor, constant: 18).isActive = true
//        mainImageView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -18).isActive = true
        mainImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 18).isActive = true
        mainImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -18).isActive = true
    }
    
    private func setupBlurredEffectViewConstraints() {
        blurredEffectView.translatesAutoresizingMaskIntoConstraints = false
        blurredEffectView.bottomAnchor.constraint(equalTo: mainImageView.bottomAnchor).isActive = true
        blurredEffectView.leftAnchor.constraint(equalTo: mainImageView.leftAnchor).isActive = true
        blurredEffectView.rightAnchor.constraint(equalTo: mainImageView.rightAnchor).isActive = true
        blurredEffectView.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    private func setupNameLabelConstraints() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.leftAnchor.constraint(equalTo: blurredEffectView.leftAnchor, constant: 8).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: blurredEffectView.rightAnchor, constant: -8).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: blurredEffectView.centerYAnchor).isActive = true
    }
    
    func setupCell(data: CatData, image: UIImage?) {
        mainImageView.image = image
        
        if let names = data.names {
            nameLabel.text = names
            
            nameLabel.isHidden = false
            blurredEffectView.isHidden = false
        } else {
            nameLabel.isHidden = true
            blurredEffectView.isHidden = true
        }
        
        if let constraint = mainImageViewHeightConstraint {
            mainImageView.removeConstraint(constraint)
        }
        
        mainImageViewHeightConstraint = mainImageView.heightAnchor.constraint(equalTo:  mainImageView.widthAnchor, multiplier: data.imageRatio)
        mainImageViewHeightConstraint?.isActive = true
    }
}
