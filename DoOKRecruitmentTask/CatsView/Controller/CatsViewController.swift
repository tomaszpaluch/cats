//
//  ViewController.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 16/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import UIKit

class CatsViewController: UIViewController {
    
    private let logic: CatsLogic
    private let tableView: UITableView
    
    init() {
        logic = CatsLogic()
        tableView = UITableView(frame: .zero)
        
        super.init(nibName: nil, bundle: nil)
        
        logic.registerCell(for: tableView)
        
        tableView.rowHeight = UIScreen.main.bounds.width

        tableView.delegate = logic
        tableView.dataSource = logic
                
        logic.reloadTableView = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        logic.reloadRow = { [weak self] rowIndex in
            DispatchQueue.main.async {
                self?.tableView.reloadRows(at: [IndexPath(row: rowIndex, section: 0)], with: .fade)
            }
        }
        
        logic.showController = { [weak self] controller in
            self?.present(controller, animated: true)
        }
        
        view.addSubview(tableView)
        
        setupTableViewConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupTableViewConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
}

