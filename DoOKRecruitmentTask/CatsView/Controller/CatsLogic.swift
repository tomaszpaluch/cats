//
//  CatsLogic.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 16/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import UIKit

class CatsLogic: NSObject {
    var reloadTableView: (() -> Void)? {
        didSet {
            if catsDataSource.catCount > 0 {
                reloadTableView?()
            }
        }
    }
    
    var reloadRow: ((Int) -> Void)?
    var showController: ((UIViewController) -> Void)?
    
    private let catsDataSource: CatsDataSource
    private let catTimer: CatTimer
    
    private let cellIdentifier: String
    
    private var reloadModalView: (() -> Void)?

    override init() {
        catsDataSource = CatsDataSource()
        catTimer = CatTimer()
        cellIdentifier = "catCell"
        
        super.init()
        
        setupCompletionHandlers()
    }
    
    init(dataSource: CatsDataSource) {
        self.catsDataSource = dataSource
        catTimer = CatTimer()
        cellIdentifier = "catCell"

        super.init()
        
        setupCompletionHandlers()
    }
    
    private func setupCompletionHandlers() {
        catsDataSource.reloadRow = { [weak self] row in
            self?.reloadRow?(row)
            self?.reloadModalView?()
        }
        
        catTimer.callback = { [weak self] in
            self?.catsDataSource.abortImagesFetching()
            self?.loadCats()
        }
    }
    
    private func loadCats() {
        catsDataSource.fetchCats { [weak self] result in
            switch result {
            case .success(_):
                self?.reloadTableView?()
                self?.reloadModalView?()
            case .failure(let error):
                self?.prepareErrorAlert(for: error)
            }
        }
    }
    
    private func prepareErrorAlert(for error: Error) {
        DispatchQueue.main.async { [weak self] in
            let alert = UIAlertController(title: "Error occured", message: error.localizedDescription, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel) { _ in
                self?.catTimer.isEnabled = true
            }
            
            alert.addAction(okAction)
            
            self?.catTimer.isEnabled = false
            self?.showController?(alert)
        }
    }
    
    func registerCell(for tableView: UITableView) {
        tableView.register(CatCell.self, forCellReuseIdentifier: cellIdentifier)
    }
}

extension CatsLogic: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        catsDataSource.catCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        print("BIORĘ CELLKĘ \(indexPath.row)")
        
        if let cell = cell as? CatCell {
            let (catData, image) = catsDataSource.getCatData(forCatAt: indexPath.row)
            cell.setupCell(data: catData, image: image)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let (catData, _) = self.catsDataSource.getCatData(forCatAt: indexPath.row)

        return ((UIScreen.main.bounds.width - 2 * 18) * catData.imageRatio) + 2 * 18
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let (catData, image) = self.catsDataSource.getCatData(forCatAt: indexPath.row)

        let controller = CatDetailViewController(data: catData, with: image)
        
        reloadModalView = {
            let (_, image) = self.catsDataSource.getCatData(forCatAt: indexPath.row)
            DispatchQueue.main.async {
                controller.reloadImage(with: image)
            }
        }
        
        controller.whenViewDisappers = { [weak self] in
            self?.reloadModalView = nil
            self?.catTimer.isEnabled = true
        }
        
        controller.openWikipedia = {
            if let url = catData.wikiURL {
                UIApplication.shared.open(url)
            }
        }
        
        catTimer.isEnabled = false
        showController?(controller)
    }
}
