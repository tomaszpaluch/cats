//
//  ImageProvider.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 21/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import UIKit

class ImageProvider {
    private let operationQueue: OperationQueue
    
    private var pathsProcessing: [String]
    
    init() {
        operationQueue = OperationQueue()
        pathsProcessing = []
    }
    
    func getImage(from path: String, completionHandler: @escaping (UIImage) -> Void) {
        if !pathsProcessing.contains(path), let url = URL(string: path) {
            let session = URLSession(configuration: .default, delegate: nil, delegateQueue: operationQueue)
            pathsProcessing.append(path)
            
            session.dataTask(with: url) { [weak self] data, response, error in
                if let data = data, let image = UIImage(data: data) {
                    completionHandler(image)
                }
                
                self?.pathsProcessing.removeAll { $0 == path }
            }.resume()
        }
    }
    
    func abort() {
        operationQueue.cancelAllOperations()
    }
}
