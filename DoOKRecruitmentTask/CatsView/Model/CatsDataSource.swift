//
//  CatsDataSource.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 21/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import UIKit

class CatsDataSource {
    var reloadRow: ((Int) -> Void)?
    
    var catCount: Int {
        catData.count
    }
    
    subscript(index: Int) -> CatData {
        get {
            catData[index]
        }
    }
    
    private(set) var catData: [CatData]
    
    private let broker: CatsBroker
    private let images: ImagesDataSource
    
    init() {
        broker = CatsBroker()
        images = ImagesDataSource()
        
        catData = []
    }
    
    init(broker: CatsBroker) {
        self.broker = broker
        images = ImagesDataSource()
        
        catData = []
    }
    
    func fetchCats(completion: @escaping ((Result<Void, Error>) -> Void)) {
        broker.fetchCats { [weak self] result in
            switch result {
            case .success(let data):
                self?.catData = data
                completion(.success(()))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getCatData(forCatAt index: Int) -> (CatData, UIImage?)  {
        let data = catData[index]
        let image = images.getImage(for: data) { [weak self] in
            self?.reloadRow?(index)
        }
        
        return (data, image)
    }
    
    func abortImagesFetching() {
        images.abortCurrentOperations()
    }
}
