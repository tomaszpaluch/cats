//
//  CatData.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 21/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import UIKit

struct CatData: Codable {
    let breeds: [CatBreed]
    let id: String
    let url: String
    let width: Int
    let height: Int
    
    var imageRatio: CGFloat {
        if height == 0 || width == 0 {
            return 1
        } else {
            return (CGFloat(height) / CGFloat(width))
        }
    }
    
    var names: String? {
        if breeds.isEmpty {
            return nil
        } else {
            return breeds.compactMap { $0.name }.reduce("") { (result, name) -> String in
                if result == "" {
                    return name
                } else {
                    return result + ", " + name
                }
            }
        }
    }
    
    var wikiURL: URL? {
        if let path = breeds.first?.wikipedia_url, let url = URL(string: path) {
            return url
        } else {
            return nil
        }
    }
    
    var descriptions: String? {
        let tempText = breeds.filter { $0.name != nil }.compactMap { ($0.name!) + ": " + ($0.description ?? "n/a") }.reduce("") { (result, description) -> String in
            if result == "" {
                return description
            } else {
                return result + "\n " + description
            }
        }
        
        if tempText.isEmpty {
            return nil
        } else {
            return tempText
        }
    }
}

struct CatBreed: Codable {
    let id: String
    let name: String?
    let wikipedia_url: String?
    let description: String?
}
