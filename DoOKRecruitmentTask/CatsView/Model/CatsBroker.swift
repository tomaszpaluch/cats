//
//  CatsBroker.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 21/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import Foundation

class CatsBroker {
    private let provider: CatsProvider
    
    init() {
        provider = CatsProvider()
    }
    
    init(provider: CatsProvider) {
        self.provider = provider
    }
    
    func fetchCats(completion: @escaping (Result<[CatData], Error>) -> Void) {
        provider.fetchCats { result in
            switch result {
            case .success(let data):
                do {
                   let catData = try JSONDecoder().decode([CatData].self, from: data)
                    completion(.success(catData))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
