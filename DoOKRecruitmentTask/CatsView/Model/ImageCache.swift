//
//  ImageCache.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 21/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import UIKit

class ImageCache {
    private var files: [String]
    
    private let cacheFolderURL: URL
    private let maximumDataCount: Int

    init() {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        cacheFolderURL = documentsURL.appendingPathComponent("cache")
        
        if FileManager.default.fileExists(atPath: cacheFolderURL.path), let urls = try? FileManager.default.contentsOfDirectory(at: cacheFolderURL, includingPropertiesForKeys: nil) {
            files = urls.compactMap { $0.path }
        } else {
            files = []
        }
        
        maximumDataCount = 100
    }
    
    func getImage(from path: String) -> UIImage? {
        if files.contains(path), let image = UIImage(contentsOfFile: path) {
            return image
        } else {
            return nil
        }
    }
    
    func addImage(_ image: UIImage, from path: String) {
        if let data = image.pngData() {
            let fileURL = cacheFolderURL.appendingPathComponent(path)
            try? data.write(to: fileURL)
            
            files.append(fileURL.path)
            
            while files.count > maximumDataCount {
                if let lastFile = files.last {
                    let fileURL = cacheFolderURL.appendingPathComponent(lastFile)
                    
                    try? FileManager.default.removeItem(at: fileURL)
                    files.removeLast()
                }
            }
        }
    }
}
