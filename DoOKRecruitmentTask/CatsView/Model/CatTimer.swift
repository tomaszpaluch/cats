//
//  CatTimer.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 21/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import UIKit

class CatTimer {
    var callback: (() -> Void)?
    
    var isEnabled: Bool

    private let refreshSecRate: Int
    
    private var currentTick: Int
    private var displayLink: CADisplayLink?

    init(refreshSecRate: Int = 20) {
        self.refreshSecRate = refreshSecRate
        currentTick = 0
        
        isEnabled = true
        
        displayLink = CADisplayLink(target: self, selector: #selector(tick))
        displayLink?.add(to: .main, forMode: .common)
        displayLink?.preferredFramesPerSecond = 1
    }
    
    @objc private func tick() {
        if isEnabled {
            if currentTick % refreshSecRate == 0 {
                currentTick = 0
                callback?()
            }
            
            currentTick += 1
        }
    }
    
    func finish() {
        displayLink?.remove(from: .main, forMode: .common)
        displayLink?.invalidate()
        displayLink = nil
    }
    
    deinit {
        print("CatTimer said bye-bye")
    }
}
