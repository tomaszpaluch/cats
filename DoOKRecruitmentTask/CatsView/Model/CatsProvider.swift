//
//  CatsProvider.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 21/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import Foundation

class CatsProvider {
    private let url: URL
    
    init() {
        url = URL(string: "https://api.thecatapi.com/v1/images/search?limit=5")!
    }

    func fetchCats(completion: @escaping (Result<Data, Error>) -> Void) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            print(self.url)
            
            if let data = data {
                completion(.success(data))
            } else if let error = error {
                completion(.failure(error))
            } else {
                completion(.failure(DataRequestError()))
            }
        }.resume()
    }
}

class DataRequestError: LocalizedError {
    public var errorDescription: String? {
        NSLocalizedString("Problems with getting data", comment: "DataRequestError")
    }
}
