//
//  ImagesDataSource.swift
//  DoOKRecruitmentTask
//
//  Created by Tomasz Paluch on 21/12/2021.
//  Copyright © 2021 Tomasz Paluch. All rights reserved.
//

import UIKit

class ImagesDataSource {
    struct ImageData {
        let date: Date
        let image: UIImage
        
        init(image: UIImage) {
            self.image = image
            date = Date()
        }
    }
        
    private(set) var data: [String: ImageData]
    
    private let imageCache: ImageCache
    private let imageProvider: ImageProvider
    
    private let maximumDataCount: Int
    
    init() {
        data = [:]
        
        imageCache = ImageCache()
        imageProvider = ImageProvider()
        
        maximumDataCount = 50
    }
    
    func getImage(for cat: CatData, refresh: @escaping () -> Void) -> UIImage? {
        if let image = data[cat.url]?.image {
            return image
        } else if let image = imageCache.getImage(from: cat.url) {
            addImage(image, for: cat.url)
            return image
        } else {
            imageProvider.getImage(from: cat.url) { [weak self] image in
                self?.data[cat.url] = ImageData(image: image)
                self?.imageCache.addImage(image, from: cat.url)
                
                print("KOTEŁ: \(cat.id) POBRANY")
                
                refresh()
            }
            
            return nil
        }
    }
    
    private func addImage(_ image: UIImage, for path: String) {
        data[path] = ImageData(image: image)
        
        if data.count > maximumDataCount, let lastElement = data.values.sorted(by: { $0.date > $1.date}).last, let keyToBeDeleted = data.first(where: { $0.value.image === lastElement.image })?.key {
            data.removeValue(forKey: keyToBeDeleted)
        }
    }
    
    func abortCurrentOperations() {
        imageProvider.abort()
    }
}
